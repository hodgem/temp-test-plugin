
if (typeof CCF === 'undefined') {
	CCF = {};
}
if (typeof CCF.subjectids === 'undefined') {
	CCF.subjectids = { config: undefined, segments: undefined, templates: undefined};
}

CCF.subjectids.initialize = function() {

	if (typeof CCF.subjectidsconfig === 'undefined' || typeof CCF.subjectidsconfig.configuration === 'undefined' ||
	    CCF.subjectids.isInitialized) {
		return;
	}
	if (typeof CCF.subjectidsconfig.configuration.ccfSubjectIdsEnabled !== 'undefined' &&
		 CCF.subjectidsconfig.configuration.ccfSubjectIdsEnabled.toString() !== 'false') {
		this.config = CCF.subjectidsconfig.configuration;
		this.template = this.config.ccfSubjectIdsTemplate;
		this.segments = CCF.subjectidsconfig.getSegments(this.template);
		$("#subjectids_item").show();
	}

}

CCF.subjectids.generate = function() {

	// Re-initialize if configuration has been updated
	if (CCF.subjectidsconfig.updated) {
		CCF.subjectidsconfig.refreshData();
		this.initialize();
	}
	if (typeof CCF.subjectidsconfig.configuration.ccfSubjectIdsEnabled == 'undefined' ||
		 CCF.subjectidsconfig.configuration.ccfSubjectIdsEnabled.toString() == 'false') {
		xmodal.message("Error", "The subject IDs plugin is not currently configured to be used for this project");
		$("#subjectids_item").hide();
		return;
	}

	if (this.segments.length<1) {
		this.newId([]);
	} else {
		this.buildSupplyUserDataModal();
	}

}

CCF.subjectids.newId = function(sendData) {

	$.ajax({
		type : "POST",
 		url:serverRoot+'/xapi/projects/' + XNAT.data.context.project +'/ccfSubjectIds/getNewId?XNAT_CSRF=' + window.csrfToken ,
		cache: false,
		async: true,
		context: this,
		data: JSON.stringify(sendData),
		dataType: 'json',
		contentType: "application/json; charset=utf-8"
	}).done( function( data, textStatus, jqXHR ) {
		if (typeof data.newSubjectId !== 'undefined') {
			this.buildIdModal(data.newSubjectId);
		} else {
			xmodal.message('Error', 'ERROR:  Could not obtain new subject ID from the server');
			console.log(data);
		}
	}).fail( function( data, textStatus, error ) {
		xmodal.message('Error', 'ERROR:  Could not obtain new subject ID from the server');
		console.log(error);
	});

}

CCF.subjectids.buildSupplyUserDataModal = function() {
	var modal_contents = '<div><h2>Please supply data for the following segments of the ID:</h2>';
	modal_contents+='<ul>';
	for (var i=0; i<this.segments.length; i++) {
		var thisEle = "ccfSubjectIdsSegmentLabel" + (i+1);
		var thisLabel = this.config[thisEle];
		thisLabel = (typeof thisLabel !== 'undefined' && thisLabel.length>0) ? thisLabel : "Please supply value:";
		modal_contents += "<li><span style='margin-right:15px;display:inline-block;width:175px;'>" + thisLabel + 
			"</span><input id='supply_" + thisEle + "' class='supplyvalue inputWrapper' type='text' width='50' maxlength='" + this.segments[i].length + "' /> "
			 + this.getSegmentTypeSpan(this.segments[i]) + "</li>";
	}
	modal_contents+='</ul>';
	modal_contents+='</div>';
	this.modal_id = "requiredInformationModal";
	var modal_opts = { 
            id: this.modal_id,
            width: 640,
            height: 485,
            title: 'Information required',
            content: modal_contents,
            ok: 'show',
            okLabel: 'Generate ID',
            okClose: false,
            okAction: function(){
		if (this.validateRequiredInfoModalContent(true)) {
			var passData = [];
			var inputArr = $("[id^=supply_ccfSubjectIdsSegmentLabel]");
			// We'll use for loop in case the elements aren't returned in order (shouldn't be the case, but to be safe...)
			for (var i=1; i<=inputArr.size(); i++) {
				var ele = $("#supply_ccfSubjectIdsSegmentLabel" + i);
				passData.push($(ele).val());
			} 
			this.newId(passData);
			xmodal.close(this.modal_id);
		}
            }.bind(this),
            cancel: 'show',
            cancelLabel: 'Cancel',
        };
	xModalOpenNew(modal_opts);
	$("#supply_ccfSubjectIdsSegmentLabel1").focus();
	// Focus gets taken away by the modal, so let's do a short timeout and do request it again.
	setTimeout(function(){
		$("#supply_ccfSubjectIdsSegmentLabel1").focus();
	},250);
	$("input[id^=supply_ccfSubjectIdsSegmentLabel]").change(function() { CCF.subjectids.validateRequiredInfoModalContent(false) });
}

CCF.subjectids.validateRequiredInfoModalContent = function(forceValidate) {
	var isInError = ($(".validate-required-banner").size()>=1);
	if (!(isInError || forceValidate)) {
		return;
	}
	var anyErrors = false;
	$("[id^=supply_ccfSubjectIdsSegmentLabel]").each(function() {
		var thisId = $(this).attr("id");
		var fieldNo = parseInt(thisId.substring(thisId.length-1));
		var thisVal = $(this).val();
		var isInError = false;
		if (typeof thisVal === 'undefined' || thisVal.length<1) {
			isInError = true;
		}
		switch (CCF.subjectids.getSegmentType(CCF.subjectids.segments[fieldNo-1])) {
			case "Numeric" :
				if (!$.isNumeric(thisVal)) {
					isInError = true;
				}
				break;
			case "Alphabetic" : 
				if (!/^[a-zA-Z]+$/.test(thisVal)) {
					isInError = true;
				}
				break;
			case "Alphanumeric" : 
				if (!/^[a-zA-Z0-9]+$/.test(thisVal)) {
					isInError = true;
				}
				break;
			default: 
				break;
		}
		if (isInError) {
			anyErrors = true;
			$(this).addClass("error");
		} else {
			$(this).removeClass("error");
		}
	});
	if (!anyErrors) {
		$(".validate-required-banner").remove();
		return true;
	}
	var opts = {
            type: 'error',
            content: 'Validation error',
            effect: 'slide',
            hold: 500000,
            container: "#requiredInformationModal",
            element: {
                addClass: 'validate-required-banner',
                style: {
                    position: 'fixed',
                    top: 'initial',
                    left: 0,
                    right: 0,
                    bottom: 'auto',
                    width: '600px',
                    margin: '0 auto',
                    zIndex: 999
                }
            }
        };
	if ($(".validate-required-banner").size()<1) {
		var bnr = new XNAT.ui.banner.init(opts);
        	bnr.$banner.click(function(){ $(this).slideUp(100) });
        	bnr.render(opts.container);
	}
	return false;
}

CCF.subjectids.getSegmentType = function(segment) {
	if (typeof segment === 'undefined') {
		return;
	}
	switch (segment.charAt(0)) {
		case "#": return "Numeric";
		case "@": return "Alphabetic";
		case "$": return "Alphanumeric";
		default: return "Unknown type";
	}
}

CCF.subjectids.getSegmentTypeSpan = function(segment) {
	var segmentType = this.getSegmentType(segment);
	return "<span style='margin-left:15px;color:#BBBBBB;'>(" + segmentType + " value)</span>"; 
}

CCF.subjectids.buildIdModal = function(newId) {
	var modal_contents = '<div><h2>New subject ID:  ' + newId + '</h2></div>';
	var modal_opts = { 
            width: 560,
            height: 285,
            title: 'Subject ID',
            content: modal_contents,
            ok: 'show',
            okLabel: 'Create new subject',
            okAction: function(){
		this.createNewSubject(newId);
            }.bind(this),
            cancel: 'show',
            cancelLabel: 'Cancel',
        };
	xModalOpenNew(modal_opts);
}

CCF.subjectids.createNewSubject = function(newId) {

	$.ajax({
		type : "PUT",
 		url:serverRoot+'/data/projects/' + XNAT.data.context.project +'/subjects/' + newId + "?XNAT_CSRF=" + window.csrfToken ,
		cache: false,
		async: true,
		context: this
	}).done( function( data, textStatus, jqXHR ) {
		window.location.assign(serverRoot + "/app/action/DisplayItemAction/search_value/" + data +
			 "/search_element/xnat:subjectData/search_field/xnat:subjectData.ID/project/" + XNAT.data.context.project);
	}).fail( function( data, textStatus, error ) {
		xmodal.message('Error', 'ERROR:  Could not create new subject');
		console.log(error);

	});
}


