package org.nrg.temp.test.xapi;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import java.util.Map;

import org.nrg.framework.annotations.XapiRestController;
import org.nrg.xdat.om.XnatProjectdata;
import org.nrg.xdat.rest.AbstractXapiRestController;
import org.nrg.xdat.security.services.RoleHolder;
import org.nrg.xdat.security.services.UserManagementServiceI;
import org.nrg.xft.security.UserI;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;


/**
 * The Class TestController.
 *
 * @author Mike Hodge
 */

@XapiRestController
@Api(description = "Temp Test API")
public class TestController extends AbstractXapiRestController {
	
	@Autowired
	public TestController(UserManagementServiceI userManagementService, RoleHolder roleHolder) {
		super(userManagementService, roleHolder);
	}
	
	@ApiOperation(value = "testing",  response = String.class)
	@ApiResponses({@ApiResponse(code = 500, message = "Unexpected error")})
	@RequestMapping(value = "/test", produces = MediaType.APPLICATION_JSON_VALUE, method = RequestMethod.GET)
	public ResponseEntity<String> doTest(@RequestParam Map<String, String> params) {
		try {
			final UserI user = getSessionUser();
			return new ResponseEntity<>("Hello World!!!", HttpStatus.OK);
			
		} catch (Exception e) {
			return new ResponseEntity<>("EXCEPTION:  "+  e.toString(),HttpStatus.INTERNAL_SERVER_ERROR );
		}
	}
	
	@ApiOperation(value = "testing",  response = String.class)
	@ApiResponses({@ApiResponse(code = 500, message = "Unexpected error")})
	@RequestMapping(value = "/projects/{projectId}/test", produces = MediaType.APPLICATION_JSON_VALUE, method = RequestMethod.GET)
	public ResponseEntity<String> doTest(@PathVariable("projectId") String projectId, @RequestParam Map<String, String> params) {
		try {
			final UserI user = getSessionUser();
			final XnatProjectdata project = XnatProjectdata.getProjectByIDorAlias(projectId, user, false);
			if (project == null) {
				return new ResponseEntity<>("ERROR:  Project not found", HttpStatus.BAD_REQUEST);
			}else {
				projectId = project.getId();
			}
			if (!project.canRead(user)) {
				return new ResponseEntity<>("ERROR:  User must have read access to project to validate subject IDs.", HttpStatus.UNAUTHORIZED);
			}
			return new ResponseEntity<>("Hello World!!!", HttpStatus.OK);
			
		} catch (Exception e) {
			return new ResponseEntity<>("ERROR:  Unexpected server error (" +  e + ")",HttpStatus.INTERNAL_SERVER_ERROR );
		}
	}
	
	@ApiOperation(value = "testing",  response = String.class)
	@ApiResponses({@ApiResponse(code = 500, message = "Unexpected error")})
	@RequestMapping(value = "/projects/{projectId}/subjects/{subjectId}/test", produces = MediaType.APPLICATION_JSON_VALUE, method = RequestMethod.GET)
	public ResponseEntity<String> doTest(@PathVariable("projectId") String projectId, @PathVariable("subjectId") String subjectId, @RequestParam Map<String, String> params) {
		try {
			final UserI user = getSessionUser();
			final XnatProjectdata project = XnatProjectdata.getProjectByIDorAlias(projectId, user, false);
			if (project == null) {
				return new ResponseEntity<>("ERROR:  Project not found", HttpStatus.BAD_REQUEST);
			}else {
				projectId = project.getId();
			}
			if (!project.canRead(user)) {
				return new ResponseEntity<>("ERROR:  User must have read access to project to validate subject IDs.", HttpStatus.UNAUTHORIZED);
			}
			return new ResponseEntity<>("Hello World!!!", HttpStatus.OK);
			
		} catch (Exception e) {
			return new ResponseEntity<>("ERROR:  Unexpected server error (" +  e + ")",HttpStatus.INTERNAL_SERVER_ERROR );
		}
	}
	
	@ApiOperation(value = "testing",  response = String.class)
	@ApiResponses({@ApiResponse(code = 500, message = "Unexpected error")})
	@RequestMapping(value = "/projects/{projectId}/subjects/{subjectId}/experiments/{experimentId}/test", produces = MediaType.APPLICATION_JSON_VALUE, method = RequestMethod.GET)
	public ResponseEntity<String> doTest(@PathVariable("projectId") String projectId, @PathVariable("subjectId") String subjectId, @PathVariable("experimentId") String experimentId, @RequestParam Map<String, String> params) {
		try {
			final UserI user = getSessionUser();
			final XnatProjectdata project = XnatProjectdata.getProjectByIDorAlias(projectId, user, false);
			if (project == null) {
				return new ResponseEntity<>("ERROR:  Project not found", HttpStatus.BAD_REQUEST);
			}else {
				projectId = project.getId();
			}
			if (!project.canRead(user)) {
				return new ResponseEntity<>("ERROR:  User must have read access to project to validate subject IDs.", HttpStatus.UNAUTHORIZED);
			}
			return new ResponseEntity<>("Hello World!!!", HttpStatus.OK);
			
		} catch (Exception e) {
			return new ResponseEntity<>("ERROR:  Unexpected server error (" +  e + ")",HttpStatus.INTERNAL_SERVER_ERROR );
		}
	}
	
}
