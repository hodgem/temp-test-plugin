
if (typeof CCF === 'undefined') {
	CCF = {};
}
if (typeof CCF.subjectidsconfig === 'undefined') {
	CCF.subjectidsconfig = { CONFIG_URL: "/xapi/projects/" + XNAT.data.context.project + "/ccfSubjectIds/config", updated: false };
}

CCF.subjectidsconfig.initialize = function() {
	if (typeof CCF.subjectidsconfig.configuration === 'undefined') {
		$.ajax({
			type : "GET",
			url:serverRoot+CCF.subjectidsconfig.CONFIG_URL,
			cache: false,
			async: true,
			context: this,
			dataType: 'json'
		 })
		.done( function( data, textStatus, jqXHR ) {
			CCF.subjectidsconfig.configuration = data;
			CCF.subjectidsconfig.submitted = false;
			XNAT.spawner.spawn(CCF.subjectidsconfig.spawnConfig()).render($("#subject_ids_div")[0]);
			CCF.subjectidsconfig.showRelevantLabelBoxes();
			if (!CCF.subjectidsconfig.validateTemplate()) {
				$("#ccfSubjectIdsTemplate").css("background-color","#FFDDEE");
			} else {
				$("#ccfSubjectIdsTemplate").css("background-color","");
			}

			var currentVal = $("#ccfSubjectIdsChecksum").val();
			$("#ccfSubjectIdsChecksum option[value='']").remove();
			if (typeof currentVal === 'undefined' || currentVal == '') {
				$("#ccfSubjectIdsChecksum").val("LASTDIGITS");
			}
			var enabledVal = $("#ccfSubjectIdsEnabled").val();
			$("#ccfSubjectIdsEnabled option[value='']").remove();
			if (typeof enabledVal === 'undefined' || enabledVal == '') {
				$("#ccfSubjectIdsEnabled").val("false");
			}
			$(".panel-footer .submit").mouseover(function(eventObj) {
				CCF.subjectidsconfig.submitted = true;
			});
			$(".panel-footer .submit").click(function(eventObj) {
				CCF.subjectidsconfig.submitted = false;
				CCF.subjectidsconfig.updated = true;
			});
			$("#ccfSubjectIdsTemplate").change(function(eventObj) {
				var segments = CCF.subjectidsconfig.getSegmentsFromInput();
				if (segments.length>0) {
					var modal_opts = { 
				            width: 400,
				            height: 200,
				            title: 'User-supplied segment labels',
				            content: ((segments.length<=5) ? "Please review and/or supply user labels for components of the template that will be supplied by the user" : 
				            				"WARNING:  You have indicated more than five user-supplied segments.  Only five segments are well supported.  " +
									"You will only be able to supply labels for five segments."
							),
				            ok: 'show',
				            okLabel: 'OK',
				            okAction: function(){
								CCF.subjectidsconfig.showRelevantLabelBoxes();
								$("#ccfSubjectIdsSegmentLabel1").focus();
				            },
				            cancel: 'hide',
				            cancelLabel: 'Cancel',
				        };
					xModalOpenNew(modal_opts);
				} else {
					CCF.subjectidsconfig.showRelevantLabelBoxes();
				}
				if (!CCF.subjectidsconfig.validateTemplate()) {
					$("#ccfSubjectIdsTemplate").css("background-color","#FFDDEE");
					xmodal.message("Error","ERROR:  The template contains invalid characters.  They will not be saved with the template.");
					return;
				} else {
					$("#ccfSubjectIdsTemplate").css("background-color","");
				}
				if (CCF.subjectidsconfig.submitted) {
					$(".panel-footer .submit").click();
					CCF.subjectidsconfig.submitted = false;
				}
			});
		})
		.fail( function( data, textStatus, error ) {
			console.log("WARNING:  The configuration call for the CCF Subject IDs plugin returned an error - (", error,")"); 
			CCF.subjectidsconfig.configuration = {};
			XNAT.spawner.spawn(CCF.subjectidsconfig.spawnConfig()).render($("#subject_ids_div")[0]);
			var currentVal = $("#ccfSubjectIdsChecksum").val();
			$("#ccfSubjectIdsChecksum option[value='']").remove();
			if (typeof currentVal === 'undefined' || currentVal == '') {
				$("#ccfSubjectIdsChecksum").val("LASTDIGITS");
			}
			var enabledVal = $("#ccfSubjectIdsEnabled").val();
			$("#ccfSubjectIdsEnabled option[value='']").remove();
			if (typeof enabledVal === 'undefined' || enabledVal == '') {
				$("#ccfSubjectIdsEnabled").val("false");
			}
		});
	}
}

CCF.subjectidsconfig.validateTemplate = function() {
	var templVal = $("#ccfSubjectIdsTemplate").val();
	return /^[a-zA-Z0-9._\-?@#$%!]+$/.test(templVal); 
}

CCF.subjectidsconfig.refreshData = function() {
	$.ajax({
		type : "GET",
		url:serverRoot+CCF.subjectidsconfig.CONFIG_URL,
		cache: false,
		async: false,
		context: this,
		dataType: 'json'
	 })
	.done( function( data, textStatus, jqXHR ) {
		CCF.subjectidsconfig.configuration = data;
		CCF.subjectidsconfig.submitted = false;
		CCF.subjectidsconfig.updated = false;
	})
	.fail( function( data, textStatus, error ) {
		// Do nothing
	});
}

CCF.subjectidsconfig.showRelevantLabelBoxes = function() {
	var segments = CCF.subjectidsconfig.getSegmentsFromInput();
	$('[id^=ccfSubjectIdsSegmentLabel]').each(function(iter) {
		if ((iter+1)>segments.length) {
			$(this).val('')
			$("div[data-name='" + $(this).attr('id') + "']").hide();
		} else {
			$("div[data-name='" + $(this).attr('id') + "']").show();
		}
	});
}


CCF.subjectidsconfig.getSegmentsFromInput = function() {
	var value = $("#ccfSubjectIdsTemplate").val();
	return this.getSegments(value);
}

CCF.subjectidsconfig.getSegments = function(value) {
	var segments = [];
	var thisSeg = "";
	for (var i=0; i<value.length; i++) {
		var thisChar = value.charAt(i);
		var nextChar = (i<value.length-1) ? value.charAt(i+1) : " ";
		thisSeg+=thisChar;
		if (thisChar !== nextChar) {
			if (thisChar == '@' || thisChar == '#') {
				segments.push(thisSeg);
			}
			thisSeg='';
		}
	}
	return segments;
}

CCF.subjectidsconfig.spawnConfig = function() {

	function configPanel(contents) {
		return {
			id: 'ccfSubjectIdsPanel',
			kind: 'panel.form',
			label: 'Subject IDs Configuration',
			header: false,
			method: "POST",
			/*
			contentType: "json",
			*/
			action: CCF.subjectidsconfig.CONFIG_URL,
			load: "CCF.subjectidsconfig.configuration",
			refresh: CCF.subjectidsconfig.CONFIG_URL,
			contents: {
				"Enabled": enabled(),
				"CheckSum Method": checksum(),
				"Subject ID Template": template(),
				"Label1": segmentLabel(1),
				"Label2": segmentLabel(2),
				"Label3": segmentLabel(3),
				"Label4": segmentLabel(4),
				"Label5": segmentLabel(5)
			}
		}
	}
	function enabled() {
		return {
			id: 'ccfSubjectIdsEnabled',
			kind: 'panel.select.init',
			name: 'ccfSubjectIdsEnabled',
			label: 'Enabled?',
			options: {
				"TRUE": {
					 "label": "True",
					 "value": "true"
				},
				"FALSE": {
					 "label": "False",
					 "value": "false"
				}
			}
		}
		/*
		return {
			kind: 'panel.input.checkbox',
			id: 'ccfSubjectIdsEnabled',
			name: 'ccfSubjectIdsEnabled',
			value: 'true',
			label: 'Enabled'
		}
		*/
	}
	function checksum() {
		return {
			id: 'ccfSubjectIdsChecksum',
			kind: 'panel.select.init',
			name: 'ccfSubjectIdsChecksum',
			label: 'CheckSum Method',
			options: {
				"MOD9710": {
					 "label": "ISO 7064 MOD 97,10",
					 "value": "MOD9710"
				},
				"LASTDIGITS": {
					 "label": "LastDigits from Multiplier",
					 "value": "LASTDIGITS"
				}
			}
		}
	}
	function template() {
		return {
			kind: 'panel.input.text',
			id: 'ccfSubjectIdsTemplate',
			name: 'ccfSubjectIdsTemplate',
			label: 'Subject ID Template',
			value: '',
			placeholder: 'CCF#!!!!&&',
			description: "The template may contain letters, numbers, dashes, underscores, periods and special characters as follows:" +
					"<br><ul>" +
					"<li>@ - An alphabetic character to be supplied by the user</li>" +
					"<li># - An integer to be supplied by the user</li>" +
					"<li>$ - An alphanumeric character to be supplied by the user</li>" +
					"<li>! - A random digit (the group of random digits will form a random number</li>" +
					"<ul><li>- e.g. 5472 from !!!!<li></ul>" +
					"<li>% - A sequential digit (the group of sequential digits will form a sequential number (e.g. 0001 from @@@@))</li>" +
					"<ul><li>- e.g. 0001 from @@@@</li></ul>" +
					"<li>? - A checksum digit</li>" +
					"</ul>"
					,
			element: {
				onchange: function(){
					// console.log(this.value)
				}
			}
		}
	}
	function segmentLabel(num) {
		var whichone = (function() {switch(num) {
				case 1: return 'first';
				case 2: return 'second';
				case 3: return 'third';
				case 4: return 'fourth';
				case 5: return 'fifth';
				default: return 'xxxx';
				}})();
		return {
			kind: 'panel.input.text',
			id: 'ccfSubjectIdsSegmentLabel' + num,
			name: 'ccfSubjectIdsSegmentLabel' + num,
			label: 'Label for ' + whichone + ' user-defined segment',
			placeholder: 'Subject Age Group (1,2,3,4)',
			/*description: 'Segment <b>TO_BE_ADDED</b>',*/
			element: {
				onchange: function(){
					// console.log(this.value)
				}
			}
		}
	}
	return {
		root: configPanel()
	};	

}

