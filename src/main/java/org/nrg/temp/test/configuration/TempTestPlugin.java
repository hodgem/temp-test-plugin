package org.nrg.temp.test.configuration;

import org.apache.log4j.Logger;
import org.nrg.framework.annotations.XnatPlugin;
import org.springframework.context.annotation.ComponentScan;

/**
 * The Class TempTestPlugin.
 */
@XnatPlugin(
				value = "tempTestPlugin",
				name = "Temp Test Plugin"
			)
@ComponentScan({
	"org.nrg.temp.test",
	"org.nrg.temp.test.xapi"
	})
public class TempTestPlugin {
	
	/** The logger. */
	public static Logger logger = Logger.getLogger(TempTestPlugin.class);

	/**
	 * Instantiates a new ccf subject ids plugin.
	 */
	public TempTestPlugin() {
		logger.info("Configuring Temp Test plugin");
	}
}
